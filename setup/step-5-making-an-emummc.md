# Step 5: Making an emuMMC

> An emuMMC is a copy of the system files on the console's internal memory. Using it, you can use Galactic without a ban risk, as long as emummc.txt stays in the `atmosphere/hosts/` folder on the SD card. It is recommended to use emuMMC for Galactic except in very specific edge cases. We are not responsible if you get banned!

1. Inject hekate
2. Tap emuMMC
3. Tap Create emuMMC
4. Tap SD Partition
5. Tap Part 1

