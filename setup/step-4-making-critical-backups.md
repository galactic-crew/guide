---
description: Do not skip this step! This is extremely important!
---

# Step 4: Making critical backups

## Making a NAND backup

1. Enter RCM and inject the Hekate payload
2. Use the touch screen to navigate to `Tools` and then `Backup eMMC`
3. Tap on `eMMC BOOT0 & BOOT1`

* This should only take a few seconds, but if your SD card is very slow, it may take around a minute.

&#x20;4\. Tap on `Close` to continue, then tap on `eMMC RAW GPP`

* This will take a long time. Expect it to take between 10 minutes to an hour (or more, if your SD card is very slow). For me, it usually takes around 20 or 21 minutes according to hekate's timer.



5\. Press `Close` > `Home` > `Power Off`

6\. Insert your SD card into your PC

7\. Copy the `backup` folder on your SD card to a safe place, preferably the same place you copied your `Nintendo` folder and the rest of the SD card's files.



## Dumping keys



1. Enter RCM and inject Lockpick\_RCM.
2. If Lockpick\_RCM asks you to select between SysNAND or EmuNAND, choose SysNAND by navigating with the volume buttons and pressing the power button. If not, continue with step 4.
3. Lockpick\_RCM should now inform you that your keys have been saved to `/switch/something.keys` on the SD card.
4. Press any button to return to the main menu.
5. Navigate to 'Power off' with the volume buttons and select it with the power button.
6. Insert your SD card into your PC.
7. Copy every file ending in`.keys` from the `switch` folder on your SD card to a safe place (it is suggested to copy it to the same place that you copied your SD card files, NAND backup and `Nintendo` folder to).
