---
description: >-
  This page will have you create a "partition" to store Galactic's system files
  on.
---

# Step 2: Partitioning the microSD card

1. Inject TegraExplorer.bin or inject using the TegraExplorer button
2. Navigate to `Partition the sd`
3. Select `Fat32+Emummc`
4. Select `Yes`
5. Press Power or B to return to the menu
6. Select  `Reboot to RCM`
