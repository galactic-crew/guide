# Step 1: Backup your SD card

If you've used your SD card to download games on your Switch, you'll need to do the following:

* Insert the Switch's SD card into your other device
* Copy everything on the card to a safe location.
* Delete all the files from the SD card.
