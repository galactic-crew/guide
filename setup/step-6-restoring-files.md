---
description: This page will instruct you on how to restore downloaded games to sysMMC.
---

# Step 6: Restoring files

* Copy the `Nintendo` folder back to your SD card, if you backed it up in Step 1.
* If you backed anything else up in Step 1, also put it back.
