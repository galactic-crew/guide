# Step 2: Copy Galactic to the console

> If you haven't followed [downloading-files.md](../prerequisites/downloading-files.md "mention") yet, please do so now.

Extract the Galactic file you downloaded to the SD card. The SD card should now look something like this in your file manager:

![The folder structure!](../.gitbook/assets/image.png)

