---
description: This page lists useful things a Switch with homebrew and hekate can do
---

# Useful things

* Run Android. Yes, you read that correctly. You can install Android or Android TV to make your Switch a tablet or a set-top box device. To set it up, see Switchroot's guide at [https://wiki.switchroot.org](https://wiki.switchroot.org).
*   Run Linux, turning your console into a small, portable desktop computer. For install guides and downloads, see [https://wiki.switchroot.org](https://wiki.switchroot.org)

    * Access a web browser. The Switch has a built-in web browser, and although limited, can be used to browse the internet with either a custom DNS or a homebrew app. Be warned: only use the custom DNS if you do not have CFW.


* There is a custom icon pack for hekate with a lot of icons you can use. Using them is outside the scope of this guide, but they are available at [https://gitlab.com/galactic-crew/icons](https://gitlab.com/galactic-crew/icons)
