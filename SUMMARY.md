# Table of contents

* [Welcome!](README.md)

## Prerequisites

* [Checking if your console is supported](prerequisites/checking-if-your-console-is-supported.md)
* [Buying supplies](prerequisites/buying-supplies.md)
* [Downloading files](prerequisites/downloading-files.md)

## Setup

* [Step 1: Backup your SD card](setup/step-1-backup-your-sd-card.md)
* [Step 2: Partitioning the microSD card](setup/step-2-partitioning-the-microsd-card.md)
* [Step 3: Copy Galactic to the console](setup/step-3-copy-galactic-to-the-console.md)
* [Step 4: Making critical backups](setup/step-4-making-critical-backups.md)
* [Step 5: Making an emuMMC](setup/step-5-making-an-emummc.md)
* [Step 6: Restoring files](setup/step-6-restoring-files.md)

***

* [Updating](updating.md)
* [Inject a payload](inject-a-payload.md)
* [Sigpatches](sigpatches.md)
* [Useful things](useful-things.md)
