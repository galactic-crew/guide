---
description: This page will guide you through injecting a payload.
---

# Inject a payload

## Windows

1. Install and run TegraRcmGUI.
2. Navigate to the `Settings` tab, then press `Install Driver` and follow the on-screen instructions.
3. Connect your Switch in RCM to your PC using the USB cable.
4. Navigate to the `Payload` tab of TegraRcmGUI.
   * Your Switch should be shown as detected in the bottom left corner.
5. Press the file button next to `Inject payload`, and navigate to and select your payload `.bin` file.
6. Click `Inject payload` to launch the payload you selected.

## Other devices



1. Go to [https://switchgui.de/web-payload](https://switchgui.de/web-payload)and scroll all the way down
2. Select an option or upload a payload from your computer.
3. Connect your Switch in RCM to your device using the USB cable.
4. Select "Do the thing". A pop up will appear. Click the `APX` option.
5. Press the Connect button and the payload will be injected.

\
