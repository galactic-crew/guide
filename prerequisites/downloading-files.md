---
description: This step will have you download all of the programs and files you need.
---

# Downloading files

1. A payload injector program

* For Windows, pick [https://github.com/eliboa/TegraRcmGUI/releases](https://github.com/eliboa/TegraRcmGUI/releases). You will need to download the required payloads however.
* For macOS, Linux/GNU, Android, and Chrome OS, use [https://switchgui.de/web-payload](https://switchgui.de/web-payload) on a browser listed in green at [https://caniuse.com/WebUSB](https://caniuse.com/WebUSB).
* For iOS, buy a computer and/or an actual phone that runs Android.

2\. Galactic's SD card files from [https://gitlab.com/galactic-crew/galactic-releases/-/releases](https://gitlab.com/galactic-crew/galactic-releases/-/releases). Get the first one at the top. If you're using Windows, you'll need to also download `payloads.zip`.

3\. Popcorn, a spinny fabric chair and a charger :smile:
