---
description: This page will have you ensure Galactic is supported by your Switch
---

# Checking if your console is supported

Galactic relies on an _exploit_ to run custom software. Newer consoles with a red box, the Switch Lite, and the OLED model Switch aren't supported. but there are also some patched consoles that look the same as their unpatched counterparts. The only way to know for sure is to check your console's serial number.

Go to [https://ismyswitchpatched.com](https://ismyswitchpatched.com) and follow the instructions to check your console.

If it says "Not patched", you can run Galactic

If it says "Potentially patched", see Testing yellow consoles

If it says "Definitely patched", you can't continue.



> If you don't have a serial sticker (like me), you need to open Settings, then select System, then select Serial Information.

