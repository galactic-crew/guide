---
description: You need a few things to get started
---

# Buying supplies

> I highly recommend buying from a retail store rather than online for everything you can. :relaxed:

The exploit used (fusee-gelee aka CVE-2018-6242) requires a few things:

* A nice USB cable. Anker is a good choice for this. It needs to be A-male to C-male.
* A jig from [https://switchjigs.com](https://switchjigs.com). AliExpress jigs also work (and is what I use) but these are all tested to work, and are much higher quality.
* A nice microSD card. It needs to be at least 64 GB, but I would splurge for 128 if at all possible to have more space for homebrew, and, if you so choose, other operating systems. Generic brands are fine, but this NEEDS to be bought at a retail store or Amazon. Counterfeit cards are everywhere.
