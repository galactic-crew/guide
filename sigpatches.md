---
description: '[redacted]'
---

# Sigpatches

Sigpatches allow you to dump games you own to emuMMC.

To see how to dump games, see [https://suchmememanyskill.github.io](https://suchmememanyskill.github.io)

> If you do this, disregard the signature patches step in the dumping guide and instead follow Installing sigpatches below.

### Installing sigpatches

> You will need a controller

1. Boot into Galactic emuMMC
2. Open the album
3. Open AIO Switch Updater
4. Select Update sigpatches
5. Select `Supports Atmosph?re x.x.x and firmware xx.x.x`
6. Reboot
