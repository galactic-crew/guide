---
description: This page will guide you through updating Galactic
---

# Updating

> If you want to reinstall Galactic (called a cleaninstall), just wipe your SD card and follow the guide again. You can also make an emuMMC backup and simply restore it if something goes wrong

## Updating your system memory's firmware

Boot normally (without using RCM and hekate) and update via System Settings

## Updating your emuMMC firmware

> You will need a controller to do this

1. Boot into Galactic emuMMC from hekate and open the Album applet.&#x20;
2. Open AIO Switch Updater
3. Select Download firmwares
4. Select the current version of HOS installed on your sysMMC
5. When it's finished, exit AIO Switch Updater
6. Open Daybreak
7. Select Install
8. Browse to the `firmwares` folder
9. Select the firmware you just downloaded
10. Follow the on-screen instructions.

## Updating Galactic

Galactic doesn't technically get updated. Instead, each component packaged with Galactic gets updated. Just go through all of the update pages in AIO Switch Updater.



&#x20;
